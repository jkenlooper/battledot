# Battle Dot

A two player game consisting of a grid of dots.

First load of game shows the player a grid and timer counting down.  clicking
on the grid places the player's dot.  Once the tick ends the player can now see
the direction of the dot and any visible dot in front of it.

## Moving

Clicking on a cell next to the dot will show an arrow and will move the dot to
it for the next tick.

## Attacking

If dot is occupied by another dot and the player clicks on it, then depending
on the other dot's direction and if it moved determines which dot will win.

## Players

The dot color is based from the visitor's ip.  No login is used, if the window
closes then that dot will be removed eventually. Reopening/reloading the window
will start with a new player dot.

Seen dots will preserve their last known location only client side.  It's up to
the player to guess which way they went. When the dot is seen again it will
update.


api:
  player/ GET
    uid:
    color:
    playing: boolean
  dots/ GET
    -
      ticks: count of ticks this dot has been on board
      moves: count of ticks this dot has moved
      wins: total wins
      color:
      id:
  tick/ GET
    end: timestamp when this tick will end
    player:
      rotate: -1, 0, or 1
      step: 1, or 0
      moved: boolean for last tick the player moved
    dots:
      total: count of all dots on board
      nearby: count of nearby dots
      nearby_active: count of dots that moved last tick
    visible_objects: diagnol-left, straight, diagnol-right
      -
        type: dot
        id:
        color:
        distance: number of spaces in front of player
      -
        type: barrier
        id: defines what kind of barrier like corner, wall,
        distance:
      - 
        type: empty
        id:
        distance: max distance
  playing/ POST
    Post at the beginning of each tick to signal that this dot is still playing.
    If the server hasn't heard from a dot after some many ticks then it will be removed.
  move/ POST
    To submit a movement for the next tick
    direction: forward, left, right
