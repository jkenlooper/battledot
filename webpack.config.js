var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var postcssImport = require('postcss-import')
var postcssNested = require('postcss-nested')
var postcssCustomProperties = require('postcss-custom-properties')
var postcssCustomMedia = require('postcss-custom-media')
var postcssCalc = require('postcss-calc')
var postcssUrl = require('postcss-url')
var postcssSprites = require('postcss-sprites')
var autoprefixer = require('autoprefixer')
var cssnano = require('cssnano')

module.exports = {
  entry: {
    // The 'site' entry is basically the 'common' chunk, and is on every page.
    site: './src/site.js',
    // Main is the main focus of the site which is the playing area.
    main: './src/main.js'
    // Other pages would go here
    // 'how-to-play': './src/how-to-play.js',
    // other: './src/other.js',
    // test: './tests.webpack.js'
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/theme/0/',
    filename: '[name].bundle.js'
  },
  resolve: {
    modulesDirectories: ['src', 'node_modules']
  },
  externals: {
    jquery: 'jQuery',
    underscore: '_',
    backbone: 'Backbone'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.html$/,
        loader: 'dom!html'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.png$/,
        loader: 'file-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader'),
        exclude: /node_modules/
      }
    ]
  },
  plugins: [
    // CommonChunkPlugin causes error with karma
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'commons',
    //   filename: 'commons.js',
    //   minChunks: 3
    // }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new ExtractTextPlugin('[name].css', {
      allChunks: true
    })
  ],
  postcss: function (webpack) {
    return [
      postcssImport({
        addDependencyTo: webpack
      }),
      postcssNested,
      postcssCustomProperties,
      postcssCustomMedia,
      postcssCalc,
      autoprefixer,
      postcssUrl,
      cssnano
    ]
  }
}

