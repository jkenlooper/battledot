/* global chai, suite, test */
var assert = chai.assert

import Player from './player.js'

suite('Player', function () {
  test('init', function () {
    let element = document.createElement('div')
    let dotElement = document.createElement('div')
    dotElement.classList.add('Player-dot')
    element.appendChild(dotElement)
    let player = Player(element)
    assert.isDefined(player)
  })

  test('rotate', function () {
    let element = document.createElement('div')
    let dotElement = document.createElement('div')
    dotElement.classList.add('Player-dot', 'Player-dot--left')
    element.appendChild(dotElement)
    let player = Player(element)
    assert.isTrue(dotElement.classList.contains('Player-dot--left'), 'Player is facing left')

    player.rotate(-1)
    assert.isFalse(dotElement.classList.contains('Player-dot--left'), 'Player is facing left down')
    assert.isTrue(dotElement.classList.contains('Player-dot--leftDown'), 'Player is facing left down')

    player.rotate(-1)
    assert.isFalse(dotElement.classList.contains('Player-dot--leftDown'), 'Player is facing right down')
    assert.isTrue(dotElement.classList.contains('Player-dot--rightDown'), 'Player is facing right down')

    player.rotate(-1)
    assert.isTrue(dotElement.classList.contains('Player-dot--right'), 'Player is facing right')
    player.rotate(-1)
    assert.isTrue(dotElement.classList.contains('Player-dot--rightUp'), 'Player is facing right up')
    player.rotate(1)
    assert.isTrue(dotElement.classList.contains('Player-dot--right'), 'Player is facing right again')
    player.rotate(1)
    assert.isTrue(dotElement.classList.contains('Player-dot--rightDown'), 'Player is facing right down again')
  })

  test('step', function () {
    let element = document.createElement('div')
    let dotElement = document.createElement('div')
    dotElement.classList.add('Player-dot', 'Player-dot--left')
    element.appendChild(dotElement)
    let player = Player(element)
    assert.isTrue(dotElement.classList.contains('Player-dot--left'), 'Player is facing left')
    assert.deepEqual(player.point, [0, 0, 0], 'initial point')
    player.step()
    assert.deepEqual(player.point, [-1, 1, 0], 'step left')
    player.step()
    assert.deepEqual(player.point, [-2, 2, 0], 'step left')
    player.rotate(-1)
    player.step()
    assert.deepEqual(player.point, [-3, 2, 1], 'rotate left and step')
    player.step()
    assert.deepEqual(player.point, [-4, 2, 2], 'step left down')
  })
})
