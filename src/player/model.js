import MinPubSub from 'minpubsub'

export default (initialDirection) => {
  let _point = [0, 0, 0]
  let _previousDirection
  let _direction = initialDirection

  return {
    set point (point) {
      if (_point.toString() === point.toString()) {
        return
      }
      _point = point
      MinPubSub.publish('player/move', [this])
    },
    get point () {
      return _point
    },
    set direction (direction) {
      _previousDirection = _direction
      _direction = direction
      MinPubSub.publish('player/direction', [this])
    },
    get direction () {
      return _direction
    },
    get previousDirection () {
      return _previousDirection
    }
  }
}
