import playerTemplate from './player.html'
import MinPubSub from 'minpubsub'

const RIGHT_DOWN = [0, -1, 1]
const LEFT_DOWN = [-1, 0, 1]
const LEFT = [-1, 1, 0]
const LEFT_UP = [0, 1, -1]
const RIGHT_UP = [1, 0, -1]
const RIGHT = [1, -1, 0]
const deltas = [
  RIGHT_DOWN,
  LEFT_DOWN,
  LEFT,
  LEFT_UP,
  RIGHT_UP,
  RIGHT
]
const DOT_DIRECTION_CLASSNAMES = [
  'Player-dot--rightDown',
  'Player-dot--leftDown',
  'Player-dot--left',
  'Player-dot--leftUp',
  'Player-dot--rightUp',
  'Player-dot--right'
]

import {translatePoint} from '../tools.js'
import PlayerModel from './model.js'
import './player.css'

export default (element) => {
  let _element = element || playerTemplate.firstChild.cloneNode(true)
  let _dotElement = _element.getElementsByClassName('Player-dot')[0]
  // initial direction is left which is 2
  let model = PlayerModel(2)

  function onPlayerDirectionChange (_model) {
    /* Replace the previous direction className on the element with current */
    _dotElement.classList.remove(DOT_DIRECTION_CLASSNAMES[_model.previousDirection])
    _dotElement.classList.add(DOT_DIRECTION_CLASSNAMES[_model.direction])
  }

  function onPlayerMoveChange (_model) {
    translatePoint(_element, _model.point)
  }

  MinPubSub.subscribe('player/direction', onPlayerDirectionChange)
  MinPubSub.subscribe('player/move', onPlayerMoveChange)

  return {
    get element () {
      return _element
    },
    rotate (amount) {
      /* Rotate the player for the amount. either -1 for left or +1 for right */
      if (amount === 0) {
        return
      }
      let direction = model.direction + amount
      if (direction < 0) {
        direction = deltas.length - 1
      } else if (direction >= deltas.length) {
        direction = 0
      }
      model.direction = direction
    },
    step () {
      /* Move the player one cell in the current direction. */
      let point = [...model.point]
      for (let i = 0; i < 3; i++) {
        point[i] = point[i] + deltas[model.direction][i]
      }
      model.point = point
    },
    get point () {
      return model.point
    },
    getPointsForVisibleList (visibles) {
      /* Return list of points that are visible to the player. */
    }
  }
}
