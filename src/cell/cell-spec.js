/* TDD */
/* global chai, suite, test, setup, teardown */
var assert = chai.assert
import Cell from './cell.js'
import classNames from '../classNames.json'

setup(function () {
})

teardown(function () {
})

suite('Cell', function () {
  test('init with defaults', function () {
    let element = document.createElement('div')
    element.classList.add(classNames.UNKNOWN)
    let cell = Cell([0, 0, 0], element)

    assert.isDefined(cell, 'A cell')
    assert.equal(cell.model.variant, classNames.UNKNOWN, 'Initial variant')
    assert.isTrue(cell.element.classList.contains(classNames.UNKNOWN), 'The element has the variant set as a class')
  })

  test('update variant', function () {
    let element = document.createElement('div')
    element.classList.add(classNames.UNKNOWN)
    var cell = Cell([0, 0, 0], element)

    cell.model.variant = classNames.EMPTY
    assert.isTrue(cell.element.classList.contains(classNames.EMPTY), 'The element can change variant')
    assert.isFalse(cell.element.classList.contains(classNames.UNKNOWN), 'The element does not have old variant')
  })

  test('two cells', function () {
    let element1 = document.createElement('div')
    element1.classList.add(classNames.UNKNOWN)
    let element2 = document.createElement('div')
    element2.classList.add(classNames.UNKNOWN)
    var cell1 = Cell([0, 0, 0], element1)
    var cell2 = Cell([0, 1, -1], element2)

    cell1.model.variant = classNames.DOT
    cell2.model.variant = 'barrier'
    assert.equal(cell1.model.variant, classNames.DOT, 'variant match')
    assert.equal(cell2.model.variant, 'barrier', 'variant match')
    assert.isTrue(cell1.element.classList.contains(classNames.DOT), 'The element can change variant')
    assert.isTrue(cell2.element.classList.contains('barrier'), 'The element can change variant')
    assert.isFalse(cell1.element.classList.contains('barrier'), 'does not contain barrier')
    assert.isFalse(cell2.element.classList.contains(classNames.DOT), 'does not contain dot')
  })

  test('update the cell variant', function () {
    let element = document.createElement('div')
    element.classList.add(classNames.UNKNOWN)
    var cell = Cell([0, 0, 0], element)
    cell.model.variant = classNames.DOT
    cell.model.variant = 'empty'
    assert.isFalse(cell.element.classList.contains(classNames.DOT), 'Change variant removes previous variant')
    assert.isTrue(cell.element.classList.contains('empty'), 'Change variant from dot to empty')
  })
})
