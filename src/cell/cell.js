import MinPubSub from 'minpubsub'
import template from './cell.html'
import './cell.css'
import {translatePoint} from '../tools.js'
import Model from './model.js'

export default (point, element) => {
  let _element = element || (template && template.firstChild.cloneNode(true)) || document.createElement('div')
  let _model = Model(point)

  function init () {
    _element.textContent = point.toString()
    translatePoint(_element, point)
  }

  function onCellVariantChange (_model) {
    _element.classList.remove(_model.previousVariant)
    _element.classList.add(_model.variant)
  }

  init()
  let variantChangeHandle = MinPubSub.subscribe('cell/' + point.toString() + '/variant', onCellVariantChange)

  return {
    get element () {
      return _element
    },
    get model () {
      return _model
    },
    destroy () {
      MinPubSub.unsubscribe(variantChangeHandle)
    }
  }
}
