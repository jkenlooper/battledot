import MinPubSub from 'minpubsub'
import classNames from '../classNames.json'

export default (point, variant) => {
  let _point = point
  let _variant = variant || classNames.UNKNOWN
  let _previousVariant

  return {
    get point () {
      return _point
    },
    get variant () {
      return _variant
    },
    set variant (variant) {
      _previousVariant = _variant
      _variant = variant
      MinPubSub.publish('cell/' + _point.toString() + '/variant', [this])
    },
    get previousVariant () {
      return _previousVariant
    }
  }
}
