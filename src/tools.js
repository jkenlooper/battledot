export const CELL_SIDE = 34.64102

export function translatePoint (element, point) {
  /* http://stackoverflow.com/questions/2459402/hexagonal-grid-coordinates-to-pixel-coordinates?rq=1 */
  /* ( where r, g, b = point and s = length of hexagon side.
  y = 3/2 * s * b
  b = 2/3 * y / s
  x = sqrt(3) * s * ( b/2 + r)
  x = - sqrt(3) * s * ( b/2 + g )
  r = (sqrt(3)/3 * x - y/3 ) / s
  g = -(sqrt(3)/3 * x + y/3 ) / s

  r + b + g = 0
  */
  var x = Math.sqrt(3) * CELL_SIDE * (point[2] / 2 + point[0])
  var y = 3 / 2 * CELL_SIDE * point[2]
  element.style.transform = 'translate(' + x + 'px, ' + y + 'px)'
}

export function triggerCustomEvent (el, eventName, data) {
  var event
  if (window.CustomEvent) {
    event = new window.CustomEvent(eventName, {detail: data})
  } else {
    event = document.createEvent('CustomEvent')
    event.initCustomEvent(eventName, true, true, data)
  }

  el.dispatchEvent(event)
}

export function generateHexagonalCoordinates (xyz, radius) {
  /* Thanks, http://stackoverflow.com/questions/2049196/generating-triangular-hexagonal-coordinates-xyz */
  var coordinates = [xyz]
  var deltas = [[1, 0, -1], [0, 1, -1], [-1, 1, 0], [-1, 0, 1], [0, -1, 1], [1, -1, 0]]
  for (var i = 0; i < radius; i++) {
    var x = xyz[0]
    var y = xyz[1] - i
    var z = xyz[2] + i
    for (var j = 0; j < 6; j++) {
      for (var k = 0; k < i; k++) {
        x = x + deltas[j][0]
        y = y + deltas[j][1]
        z = z + deltas[j][2]
        coordinates.push([x, y, z])
      }
    }
  }
  return coordinates
}
