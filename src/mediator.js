/* Singleton */
// var asEvented = require('asevented')
var Eev = require('eev')

module.exports = (function () {
  var instance

  function init () {
    console.log('init')
    // Private methods

    // asEvented.call(Mediator.prototype)
    // var mediator = new Mediator()
    // return mediator
    var e = new Eev()
    return e

    // return {
    //   // Public methods
    //   trigger: function (topic, data) {
    //     mediator.trigger(topic, data)
    //   },

    //   bind: function (topic, callback) {
    //     mediator.bind(topic, callback)
    //   }
    // }
  }

  return {
    getInstance: function () {
      console.log('getInstance')
      if (!instance) {
        instance = init()
      }
      return instance
    }
  }
})()
