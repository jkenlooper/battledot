/* global chai, suite, test, setup, teardown, sinon */
var assert = chai.assert
// var lolex = require('lolex')
var App = require('./app.js')

// mocha.setup('tdd')

// var server
// var clock
//
// setup(function () {
//   var now = new Date()
//   server = sinon.fakeServer.create()
//   clock = lolex.install()
//   clock.setSystemTime(now)
// })
//
// teardown(function () {
//   server.restore()
//   clock.uninstall()
// })

suite.skip('App', function () {
  test('init with uid', function () {
    var el = document.createElement('div')
    var app = new App(el)
    server.respondWith('/api/v1/player/', [200, {'Content-Type': 'application/json'}, JSON.stringify({
      uid: 'abc123'
    })])
    assert.isUndefined(app.UID)
    app.initialize()
    clock.tick(1)
    server.respond()
    assert.equal(app.UID, 'abc123', 'player has uid')
  })

  test('no response from server to get player uid', function () {
    var el = document.createElement('div')
    var app = new App(el)
    server.respondWith('/api/v1/player/', [404, {}, ''])
    app.initialize()
    server.respond()
    clock.tick(1)
    assert.isUndefined(app.UID, 'no uid is set')
    console.log(app.UID)
  })
})
