/* global chai, suite, test, setup, teardown */
var assert = chai.assert
import Hextile from './hextile.js'
import {generateHexagonalCoordinates} from '../tools.js'

setup(function () {
})

teardown(function () {
})

suite('Hextile', function () {
  test('init', function () {
    let element = document.createElement('div')
    let hextile = Hextile(element)
    assert.isDefined(hextile, 'A hextile is defined')
  })

  test('add one layer of cells around the origin', function () {
    let element = document.createElement('div')
    let hextile = Hextile(element)
    hextile.addCells(generateHexagonalCoordinates([0, 0, 0], 2))
    assert.equal(element.childElementCount, 7)
    assert.isDefined(hextile.cellAtPoint([1, 0, -1]))
  })
})

