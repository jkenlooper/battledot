export default () => {
  let cellPoints = {}

  return {
    addCells (points) {
      // Add only the cells not already in the list
      let added_cells = []
      points.forEach(point => {
        if (cellPoints[point]) {
          return
        }
        cellPoints[point] = point
        added_cells.push(point)
      })
      return added_cells
    },
    pointToCellId (xyz) {
      // Useful for setting a unique identifier for the id attribute on the element
      return 'point_' + xyz.join('_')
    }
  }
}
