import template from './hextile.html'
import './hextile.css'
import Cell from '../cell/cell.js'

export default (element) => {
  let cells = {}
  let model = HextileModel()
  let _element = element || template.firstChild.cloneNode(true)

  return {
    addCells (points) {
      model.addCells(points).forEach(point => {
        let cell = Cell(point)
        cells[point] = cell
        _element.appendChild(cell.element)
      })
    },
    cellAtPoint (point) {
      return cells[point]
    },
    get element () {
      return _element
    }
  }
}

let HextileModel = () => {
  let cellPoints = {}

  return {
    addCells (points) {
      // Add only the cells not already in the list
      let added_cells = []
      points.forEach(point => {
        if (cellPoints[point]) {
          return
        }
        cellPoints[point] = point
        added_cells.push(point)
      })
      return added_cells
    },
    pointToCellId (xyz) {
      // Useful for setting a unique identifier for the id attribute on the element
      return 'point_' + xyz.join('_')
    }
  }
}
