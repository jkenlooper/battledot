// var reqwest = require('reqwest')
import Timer from './timer/index.js'
import Player from './player/player.js'
import Tick from './tick/index.js'
import Hextile from './hextile/hextile.js'
import timerTemplate from './timer/timer.html'
import {generateHexagonalCoordinates} from './tools.js'

export default (element) => {
  // let UID
  let hextile = Hextile()
  hextile.addCells(generateHexagonalCoordinates([0, 0, 0], 10))
  let player = Player()
  hextile.element.appendChild(player.element)
  let timerElement = timerTemplate.firstChild.cloneNode(true)
  let timer = Timer(timerElement)

  var fetchPlayerData = function () {
    console.log('fetchPlayerData')
    // Fetch player data from the server
    reqwest({
      url: '/api/v1/player/',
      type: 'json',
      method: 'GET',
      contentType: 'application/json'
    })
    .then(function (data) {
      console.log('Success')
      //self.UID = data.uid
      //self.tick = new Tick(data.uid, self.hextile, self.timer)
    })
    .fail(function (err, msg) {
      // error
      console.log(err.statusText, 'Retrying')
      //window.setTimeout(function () {
      //  fetchPlayerData()
      //}, 3000)
    })
    .always(function (data) {
      console.log('finished.', data)
    })
    console.log('end fetchPlayerData')
  }

  return {
    initialize () {
      // fetchPlayerData()
      hextile.addCells([[0, 0, 0], [0, -7, 7]])
      element.appendChild(hextile.element)
      element.appendChild(timerElement)
    },
    get player () {
      return player
    }
  }
}
