import App from './app.js'

let app = App(document.getElementById('board'))
app.initialize()
window.app = app

/*
board
  tick
    timer
    hextile
      cell
      player
*/
