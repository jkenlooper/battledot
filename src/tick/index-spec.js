/* global chai, suite, test, setup, teardown */

var assert = chai.assert
// var lolex = require('lolex')
import Timer from '../timer/index.js'
import Tick from './index.js'

// mocha.setup('tdd')

// var server
// var clock
let timer
let samples

setup(function () {
  let now = new Date()
  // server = sinon.fakeServer.create()
  // clock = lolex.install()
  // clock.setSystemTime(now)
  let element = document.createElement('div')
  timer = new Timer()
  samples = [
    {
      end: Math.round(now.getTime() / 1000) + 10,
      player: {
        rotate: 0,
        step: 0,
        moved: false
      },
      dots: null,
      visible_objects: [
        {
          type: 'dot',
          id: 123,
          color: '#f4ddaa',
          distance: 3
        },
        {
          type: 'barrier',
          id: 2,
          distance: 7
        },
        {
          type: 'empty',
          id: null,
          distance: 10
        }
      ]
    },
    {
      end: Math.round(now.getTime() / 1000) + 10 + 6,
      player: {
        rotate: 0,
        step: 0,
        moved: false
      },
      dots: null,
      visible_objects: [
        {
          type: 'dot',
          id: 123,
          color: '#f4ddaa',
          distance: 3
        },
        {
          type: 'barrier',
          id: 2,
          distance: 7
        },
        {
          type: 'empty',
          id: null,
          distance: 10
        }
      ]
    },
    {
      end: Math.round(now.getTime() / 1000) + 10 + 6 + 20,
      player: {
        rotate: 0,
        step: 0,
        moved: false
      },
      dots: null,
      visible_objects: [
        {
          type: 'dot',
          id: 123,
          color: '#f4ddaa',
          distance: 3
        },
        {
          type: 'barrier',
          id: 2,
          distance: 7
        },
        {
          type: 'empty',
          id: null,
          distance: 10
        }
      ]
    }
  ]
})

teardown(function () {
  // clock.uninstall()
  // server.restore()
})

suite.skip('Tick', function () {
  test('init with blank', function () {
    var tick = new Tick('abc', timer)
    assert.isDefined(tick, 'A tick is defined')
  })
})

suite.skip('server responses for tick', function () {
  test('tick end time and polling', function () {
    var tick = new Tick('abc', timer)
    var tick_url = '/api/v1/player/' + tick.uid + '/tick/'
    server.respondWith(tick_url, [200, {'Content-Type': 'application/json'}, JSON.stringify(samples[0])])
    tick.initialize()
    assert.equal(tick.count, 0, 'tick count start')
    server.respond()
    assert.equal(tick.count, 1, 'tick count updated')
    clock.tick(8000)
    assert.equal(tick.count, 1, 'tick count still the same')

    // 1 second late and server not ready
    clock.tick(3000)
    server.respondWith(tick_url, [404, {}, ''])
    server.respond()
    assert.equal(tick.count, 1, 'tick count still the same after error')

    // try again
    clock.tick(3000)
    server.respondWith(tick_url, [200, {'Content-Type': 'application/json'}, JSON.stringify(samples[1])])
    server.respond()
    assert.equal(tick.count, 2, 'tick count')

    // end time is in 2 seconds
    clock.tick(2000)
    server.respondWith(tick_url, [200, {'Content-Type': 'application/json'}, JSON.stringify(samples[2])])
    server.respond()
    assert.equal(tick.count, 3, 'tick count')

    // next tick in 20 seconds
  })

  test.skip('player move', function () {
  })
})
