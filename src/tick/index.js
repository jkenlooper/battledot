var reqwest = require('reqwest')
module.exports = (function () {
  var self

  function Tick (uid, timer) {
    self = this
    this.count = 0
    this.uid = uid
    this.timer = timer
  }

  var secondsTillNextTick = 0

  var fetchTickData = function () {
    // fetch data from server for /api/tick/
    reqwest({
      url: '/api/v1/player/' + self.uid + '/tick/',
      type: 'json',
      method: 'GET',
      contentType: 'application/json'
      // headers: {
      //   'X-My-Custom-Header': 'SomethingImportant'
      // },
    })
    .then(function (data) {
      var now = new Date()
      secondsTillNextTick = data.end - Math.round(now.getTime() / 1000)
      self.handleTickData(data)
    })
    .fail(function (err, msg) {
      // error
      // Will fail if the tick is not ready yet.  Should retry in a few seconds
      // by manually adjusting the secondsTillNextTick.
      secondsTillNextTick = 3
      console.log(err.statusText, 'Retrying')
    })
    .always(function (data) {
      console.log('Finished. Next tick in ' + secondsTillNextTick + ' seconds')
      window.setTimeout(fetchTickData, secondsTillNextTick * 1000)
    })
  }

  Tick.prototype.initialize = function () {
    // start polling the server for new tick data?
    fetchTickData()
  }

  Tick.prototype.incrTickCount = function () {
    this.count++
  }

  Tick.prototype.handleTickData = function (data) {
    this.count++
    // set the timer.end
    // hextile.player.rotate
    // hextile.player.step
    // hextile.addcells hextile.getPointsForVisibileList
  }

  return Tick
})()
